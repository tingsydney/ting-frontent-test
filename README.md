## Ting Ren - Frontend Test

This project was bootstrapped with the latest Create React App v3.0.
It is using React + Redux + Typescript.

## My explanations for the test
* First I created the project using create-react-app with --typescript parameter.
* I used `reactstrap` for UI to save some time. The whole App is responsive.
* Used Adobe XD to open the psd file to grab color, font, etc.
* I used Redux for state management for this project. Another option could be the new Context API.
* For the form, I decided to write it from scratch (including validation) without using any popular React form library, 
as I think it will be better to demonstrate my code for a test.
* Form field validation rules are not 'production-ready' due to limited time.
* Used `react-input-mask` for Phone input field.
* Followed the hCard format - [http://microformats.org/wiki/hcard](http://microformats.org/wiki/hcard) when creating HCardPreview component.
* For css, I used `css module`, another option could be to use install and use SCSS.
* There is no backend or mock API for this test, if there is an API, I could use `axios` and redux-thunk to call the API to submit data.
* I spent around 6 - 7 hours for the test, as adding Typescript required additional time.
* For Redux, React-Redux working with Typescript, I mainly referred to this official article [https://redux.js.org/recipes/usage-with-typescript](https://redux.js.org/recipes/usage-with-typescript).

## My building steps mainly includes
* Design the app component tree. I drew a quick diagram here
![App Design](./src/assets/appDesign/tingren_componentTreeWorkFlow.png "App Design")
* Design the app data flow by using Redux, mainly thought about these questions:
what states should be in the global store state, 
what should be component local states,
define Actions, Action Creators, Reducers, etc.

## Available Scripts
In the project directory, you can run:
* `npm start`
Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

* `npm test`
Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

* `npm run build`
Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

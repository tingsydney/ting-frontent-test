import {InputData} from "./actions";

// Describe the shape of the Redux store state
export interface FormState {
  givenName: string,
  surname: string,
  email: string,
  phone: string,
  houseName: string,
  street: string,
  suburb: string,
  state: string,
  postcode: string,
  country: string,
  avatar: string
}

// Describing the different ACTION NAMES available
export const SET_INPUT_FIELD = 'SET_INPUT_FIELD';
export const UPLOAD_AVATAR = 'UPLOAD_AVATAR';

interface SetInputFieldAction {
  type: typeof SET_INPUT_FIELD,
  payload: InputData
}

interface UploadAvatarAction {
  type: typeof UPLOAD_AVATAR,
  fileURL: string

}

export type AppActionTypes = SetInputFieldAction | UploadAvatarAction;

import {AppActionTypes, SET_INPUT_FIELD, UPLOAD_AVATAR} from "./types";

export interface InputData {
  value: string,
  inputName: string
}

// Action Creators
export const setInputField = (inputData: InputData): AppActionTypes => {
  return {
    type: SET_INPUT_FIELD,
    payload: inputData
  }
};

export const uploadAvatar = (fileURL: any): AppActionTypes => {
  return {
    type: UPLOAD_AVATAR,
    fileURL: fileURL
  }
};

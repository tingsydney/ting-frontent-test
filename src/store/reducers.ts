import {AppActionTypes, FormState, SET_INPUT_FIELD, UPLOAD_AVATAR} from "./types";
import avatarIcon from '../assets/images/avatar.png';

const initialState: FormState = {
  givenName: '',
  surname: '',
  email: '',
  phone: '',
  houseName: '',
  street: '',
  suburb: '',
  state: '',
  postcode: '',
  country: '',
  avatar: avatarIcon
};

// Reducers are just pure functions that take the previous state, an action and then return the next state.
export const hCardBuilderReducer = (state = initialState, action: AppActionTypes): FormState => {
  switch (action.type) {
    case SET_INPUT_FIELD:
      return {
        ...state,
        [action.payload.inputName]: action.payload.value
      };
    case UPLOAD_AVATAR:
      return {
        ...state,
        avatar: action.fileURL
      };
    default:
      return state;
  }
};

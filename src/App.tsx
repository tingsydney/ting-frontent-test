import React from 'react';
import HCardBuilder from "./containers/HCardBuilder/HCardBuilder";

const App: React.FC = () => {
  return (
      <HCardBuilder/>
  );
};

export default App;

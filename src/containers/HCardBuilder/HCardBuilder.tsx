import React from 'react';
import HCardForm from "../../components/HCardForm/HCardForm";
import HCardPreview from "../../components/HCardPreview/HCardPreview";
import {Container, Row, Col} from "reactstrap";
import classes from './HCardBuilder.module.css';

const HCardBuilder: React.FunctionComponent = () => {
  return (
      <Container className={classes.container}>
        <Row>
          <Col sm={6} className={classes.formBackground}>
            <h1 className={classes.formHeader}>hCard Builder</h1>
            <HCardForm/>
          </Col>
          <Col sm={6} className={classes.preview}>
            <HCardPreview/>
          </Col>
        </Row>
      </Container>
  );
};

export default HCardBuilder;

import React from 'react';
import classes from './PreviewField.module.css';

interface IPreviewFieldProps {
  label: string,
  value: string
}

const PreviewField: React.FunctionComponent<IPreviewFieldProps> = (props) => {
  return (
      <p className={classes.fieldContainer}>
        <span className={classes.label}>{props.label}</span>
        <span className={classes.value}>{props.value}</span>
      </p>
  );
};

export default PreviewField;

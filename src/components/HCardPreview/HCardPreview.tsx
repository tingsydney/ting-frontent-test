import React from 'react';
import * as constant from '../../constants';
import {connect} from "react-redux";
import {Card, Row, Col, CardBody, CardHeader} from 'reactstrap';
import PreviewField from "./PreviewField/PreviewField";
import classes from './HCardPreview.module.css';
import {AppState} from "../../index";

class HCardPreview extends React.Component<AppState> {

  render() {
    const {givenName, surname, email, phone, houseName, street, suburb, state, postcode, country, avatar} = this.props;
    const address = `${houseName} ${street}`;
    const addressSecond = state ? `${suburb}, ${state}` : suburb;

    return (
        <Card className="align-middle vcard">
          <div className={`text-right ${classes.preview}`}>
            HCARD PREVIEW
          </div>
          <CardHeader className={classes.previewHeader}>
            <div className={`${classes.fullName} fn`}>
              <span className="given-name">{givenName}</span> <span className="family-name">{surname}</span>
            </div>
            <div className="text-right">
              <img src={avatar} alt="Avatar" className={`${classes.avatar} photo`}/>
            </div>
          </CardHeader>
          <CardBody className={classes.previewBody}>
            <div className="email">
              <PreviewField label={constant.EMAIL} value={email}/>
            </div>
            <div className="tel">
              <PreviewField label={constant.PHONE} value={phone}/>
            </div>
            <div className="adr">
              <div className="street-address">
                <PreviewField label={constant.ADDRESS} value={address}/>
              </div>
              <div className="locality">
                <PreviewField label="" value={addressSecond}/>
              </div>

              <Row>
                <Col sm={6}>
                  <div className="postal-code">
                    <PreviewField label={constant.POSTCODE} value={postcode}/>
                  </div>
                </Col>
                <Col sm={6}>
                  <div className="country-name">
                    <PreviewField label={constant.COUNTRY} value={country}/>
                  </div>
                </Col>
              </Row>
            </div>
          </CardBody>
        </Card>
    );
  }
}

const mapStateToProps = (state: AppState): AppState => {
  return {
    givenName: state.givenName,
    surname: state.surname,
    email: state.email,
    phone: state.phone,
    houseName: state.houseName,
    street: state.street,
    suburb: state.suburb,
    state: state.state,
    postcode: state.postcode,
    country: state.country,
    avatar: state.avatar
  };
};

export default connect(mapStateToProps)(HCardPreview);

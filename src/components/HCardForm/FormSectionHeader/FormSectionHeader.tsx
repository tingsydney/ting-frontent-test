import React from 'react';
import classes from './FormSectionHeader.module.css';

interface FormSectionHeaderProps {
  title: string
}

const FormSectionHeader: React.FunctionComponent<FormSectionHeaderProps> = (props) => (
    <p className={classes.sectionHeader}>{props.title}</p>
);

export default FormSectionHeader;

import React from 'react';
import {connect} from 'react-redux';
import * as constants from '../../constants';
import {FormGroup, Label, Input, Row, Col, Button, FormFeedback} from "reactstrap";
import FormSectionHeader from "./FormSectionHeader/FormSectionHeader";
import {AppState} from "../../index";
import {Dispatch} from "redux";
import {InputData, setInputField, uploadAvatar} from "../../store/actions";
import classes from './HCardForm.module.css';
import InputMask from 'react-input-mask';

export type HCardFormProps = AppState & MapDispatchProps;

class HCardForm extends React.Component<HCardFormProps> {

  // Local component state
  state = {
    givenNameValidation: '',
    surnameValidation: '',
    emailValidation: '',
    postcodeValidation: '',
    countryValidation: '',
  };

  inputChangedHandler = (event: any, inputName: string) => {
    let value = event.target.value;

    this.validateField(value, inputName);

    let inputData: InputData = {
      value: event.target.value,
      inputName: inputName
    };
    this.props.onSetInputField(inputData);
  };

  validateField = (value: string, inputName: string) => {
    switch (inputName) {
      case 'givenName':
        if (value.length > constants.MAX_NAME_LENGTH) {
          this.setState({givenNameValidation: 'invalid'});
        } else {
          this.setState({givenNameValidation: 'valid'});
        }
        break;
      case 'surname':
        if (value.length > constants.MAX_NAME_LENGTH) {
          this.setState({surnameValidation: 'invalid'});
        } else {
          this.setState({surnameValidation: 'valid'});
        }
        break;
      case 'email':
        const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (emailRex.test(value)) {
          this.setState({emailValidation: 'valid'});
        } else {
          this.setState({emailValidation: 'invalid'});
        }
        break;
      case 'postcode':
        if (value.length > constants.MAX_POSTCODE_COUNTRY_LENGTH) {
          this.setState({postcodeValidation: 'invalid'});
        } else {
          this.setState({postcodeValidation: 'valid'});
        }
        break;
      case 'country':
        if (value.length > constants.MAX_POSTCODE_COUNTRY_LENGTH) {
          this.setState({countryValidation: 'invalid'});
        } else {
          this.setState({countryValidation: 'valid'});
        }
        break;
      default:
        break;
    }
  };

  updateAvatarHandler = (event: any) => {
    event.preventDefault();
    const file = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.onloadend = () => (
        this.props.onUploadAvatar(fileReader.result)
    );
    fileReader.readAsDataURL(file);
  };

  render() {
    return (
        <React.Fragment>
          <FormSectionHeader title="PERSONAL DETAILS"/>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label for="givenName" className={classes.inputLabel}>{constants.GIVEN_NAME}</Label>
                <Input
                    type="text"
                    name="givenName"
                    id="givenName"
                    className={classes.inputField}
                    onChange={(event) => this.inputChangedHandler(event, 'givenName')}
                    valid={this.state.givenNameValidation === 'valid'}
                    invalid={this.state.givenNameValidation === 'invalid'}
                />
                <FormFeedback valid></FormFeedback>
                <FormFeedback>
                  {constants.ERROR_MAX_NAME}
                </FormFeedback>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label for="surname" className={classes.inputLabel}>{constants.SURNAME}</Label>
                <Input
                    type="text"
                    name="surname"
                    id="surname"
                    className={classes.inputField}
                    onChange={(event) => this.inputChangedHandler(event, 'surname')}
                    valid={this.state.surnameValidation === 'valid'}
                    invalid={this.state.surnameValidation === 'invalid'}
                />
                <FormFeedback valid></FormFeedback>
                <FormFeedback>
                  {constants.ERROR_MAX_NAME}
                </FormFeedback>
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label for="email" className={classes.inputLabel}>{constants.EMAIL}</Label>
                <Input
                    type="email"
                    name="email"
                    id="email"
                    className={classes.inputField}
                    onChange={(event) => this.inputChangedHandler(event, 'email')}
                    valid={this.state.emailValidation === 'valid'}
                    invalid={this.state.emailValidation === 'invalid'}
                />
                <FormFeedback valid></FormFeedback>
                <FormFeedback>
                  {constants.ERROR_EMAIL}
                </FormFeedback>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label for="phone" className={classes.inputLabel}>{constants.PHONE}</Label>
                <Input
                    type="tel"
                    name="phone"
                    id="phone"
                    className={classes.inputField}
                    mask="02\ 9999 9999"
                    maskChar=" "
                    tag={InputMask}
                    onChange={(event) => this.inputChangedHandler(event, 'phone')}
                />
              </FormGroup>
            </Col>
          </Row>

          <FormSectionHeader title="ADDRESS"/>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label for="houseName" className={classes.inputLabel}>{constants.HOUSE_NAME}</Label>
                <Input
                    type="text"
                    name="houseName"
                    id="houseName"
                    className={classes.inputField}
                    onChange={(event) => this.inputChangedHandler(event, 'houseName')}
                />
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label for="street" className={classes.inputLabel}>{constants.STREET}</Label>
                <Input
                    type="text"
                    name="street"
                    id="street"
                    onChange={(event) => this.inputChangedHandler(event, 'street')}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label for="suburb" className={classes.inputLabel}>{constants.SUBURB}</Label>
                <Input
                    type="text"
                    name="suburb"
                    id="suburb"
                    className={classes.inputField}
                    onChange={(event) => this.inputChangedHandler(event, 'suburb')}
                />
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label for="state" className={classes.inputLabel}>{constants.STATE}</Label>
                <Input
                    type="text"
                    name="state"
                    id="state"
                    className={classes.inputField}
                    onChange={(event) => this.inputChangedHandler(event, 'state')}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label for="postcode" className={classes.inputLabel}>{constants.POSTCODE}</Label>
                <Input
                    type="text"
                    name="postcode"
                    id="postcode"
                    className={classes.inputField}
                    onChange={(event) => this.inputChangedHandler(event, 'postcode')}
                    valid={this.state.postcodeValidation === 'valid'}
                    invalid={this.state.postcodeValidation === 'invalid'}
                />
                <FormFeedback valid></FormFeedback>
                <FormFeedback>
                  {constants.ERROR_MAX_POSTCODE_COUNTRY}
                </FormFeedback>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label for="country" className={classes.inputLabel}>{constants.COUNTRY}</Label>
                <Input
                    type="text"
                    name="country"
                    id="country"
                    className={classes.inputField}
                    onChange={(event) => this.inputChangedHandler(event, 'country')}
                    valid={this.state.countryValidation === 'valid'}
                    invalid={this.state.countryValidation === 'invalid'}
                />
                <FormFeedback valid></FormFeedback>
                <FormFeedback>
                  {constants.ERROR_MAX_POSTCODE_COUNTRY}
                </FormFeedback>
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col sm={6}>
              <Label for="avatar" className={classes.uploadLabel}>Upload Avatar</Label>
              <Input
                  type="file"
                  name="avatar"
                  id="avatar"
                  className={classes.inputField}
                  style={{display: "none"}}
                  onChange={this.updateAvatarHandler}
              />
            </Col>
            <Col sm={6}>
              <Button block color="primary">Create hCard</Button>
            </Col>
          </Row>
        </React.Fragment>
    );
  }
}

interface MapDispatchProps {
  onSetInputField: (inputData: InputData) => void,
  onUploadAvatar: (fileURL: any) => void
}

const mapStateToProps = (state: AppState): AppState => {
  return {
    givenName: state.givenName,
    surname: state.surname,
    email: state.email,
    phone: state.phone,
    houseName: state.houseName,
    street: state.street,
    suburb: state.suburb,
    state: state.state,
    postcode: state.postcode,
    country: state.country,
    avatar: state.avatar
  };
};

const mapDispatchToProps = (dispatch: Dispatch): MapDispatchProps => {
  return {
    onSetInputField: (inputData: InputData) => dispatch(setInputField(inputData)),
    onUploadAvatar: (fileURL: any) => dispatch(uploadAvatar(fileURL))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(HCardForm);

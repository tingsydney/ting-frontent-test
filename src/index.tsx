import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore} from 'redux';
import {hCardBuilderReducer} from './store/reducers';
import {Provider} from 'react-redux';

// create Redux central store
// did not use combineReducers here as for now we only have one reducer for this app
// if we need to call backend apis, then will use applyMiddleware(thunk) when creating the store
const store = createStore(hCardBuilderReducer);

export type AppState = ReturnType<typeof hCardBuilderReducer>;

// connect Redux store to React
const Root = (
    <Provider store={store}>
      <App/>
    </Provider>
);

ReactDOM.render(Root, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
